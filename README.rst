Ansible django playbook
=======================

Ansible playbooks for django project to use with Semaphore.

Usage
-----

Import this repository as a Playbook repository on Semaphore.

Basic Commands
--------------

Deploying a new project
^^^^^^^^^^^^^^^^^^^^^^^

* Create a new project on Semaphore
* Add **id_rsa_semaphore** SSH key in KeyStore
* Import this repository on **Playbook repository** (git@bitbucket.org:django-social-network/ansible-django-playbooks.git)
* Create a exoscale host with **Linux Ubuntu 16.04 LTS 64-bit**
* Create an inventory and edit it::

    [servers]
    159.100.251.52

* Also create an Environnement with the same name as the inventory::

    {
      "project_name": "zeub_web",
      "git_repository": "git@bitbucket.org:wallstreetweb/zeub_web.git",
      "git_ssh_id_rsa_pub": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDX+TVYJVZPNi6T1ybOBEmrDo5H9G7k1gqgZrU2lgi1a0jDo2DS2fmdx6g9wpBn3C/C/HUxFlumQgJBLrFKyGUgcr4RDHM1/pev8Jk42uGqYmEBFJ2zyWfhQwLtxylTAtsGarEa9sHrpCfgu86qVQJwOoe/fRjjaKpKc4CfK1YN/rklAFwXVeanpr5UYAZzRJI4iwuOSHmDdIadT+041cnqPl5+v0LBVM1dRxCaEPvzu14SKjP6YDf1PuOwRRwVOleGCDV6DwrnTCvzBddynuynF3ZwuKPW0NB+iqkdHnPnu99z0sz3Tg7rgiKt+DJ1Pfxo7PYb1zZ3GWgMfZ59l803aWz4gMxMvE5AiHsLfb8kJZBx6gqaWThcU7amoveK7pOe9wNrd/SfOsNThaYVUaDsNVt0XGATzu8C562LNmltcVz5PRLZzcqTFmOrNbs39rKaSBorsKG6hVts1+ePdU90vhBFahEd5/Axw8A5udYoIfjhpmA9BLY/yzw/gy0cKZu5/UQWXA3rhrsfyUpmFuYOFNOlYFu/CyRK8r/hu4YWAi8HSiGAGSEdv00LwlT7ui6L84SYGUnwLrIKAWl6cL9f0QF4QtgWeM0707oDjzyxGeJaW1wlj9tzRW1SiM7KnVvFYK4bwB9xnF95C35GYSADfJ+KksaXoKMtYO5FWvdP7Q== arthur@macbook-pro-de-arthur-2.home\n",
      "git_ssh_id_rsa": "-----BEGIN RSA PRIVATE KEY-----\nMIIJKgIBAAKCAgEA1/k1WCVWTzYuk9cmzgRJqw6OR/Ru5NYKoGa1NpYItWtIw6Ng\n0tn5nceoPcKQZ9wvwvx1MRZbpkICQS6xSshlIHK+EQxzNf6Xr/CZONrhqmJhARSd\ns8ln4UMC7ccpUwLbBmqxGvbB66Qn4LvOqlUCcDqHv30Y42iqSnOAnytWDf65JQBc\nF1Xmp6a+VGAGc0SSOIsLjkh5g3SGnU/tONXJ6j5efr9CwVTNXUcQmhD787teEioz\n+mA39T7jsEUcFTpXhgg1eg8K50wr8wXXcp7spxd2cLij1tDQfoqpHR5z57vfc9LM\n904O64IirfgydT38aOz2G9c2dxloDH2efZfNN2ls+IDMTLxOQIh7C32/JCWQceoK\nmlk4XFO2pqL3iu6TnvcDa3f0nzrDU4WmFVGg7DVbdFxgE87vAuetizZpbXFc+T0S\n2c3KkxZjqzW7N/aymkgaK7ChuoVbbNfnj3VPdL4QRWoRHefwMcPAObnWKCH44aZg\nPQS2P8s8P4MtHCmbuf1EFlwN64a7H8lKZhbmDhTTpWBbvwskSvK/4buGFgIvB0oh\ngBkhHb9NC8JU+7oui/OEmBlJ8C6yCgFpenC/X9EBeELYFnjNO9O6A488sRniWltc\nJY/bc0VtUojOyp1bxWCuG8AfcZxfeQt+RmEgA3yfipLGl6CjLWDuRVr3T+0CAwEA\nAQKCAgAS9/RTdZQLCPs2SONDnOTLfU/JyhIjTxlH1T19Odgx9uZBKIxMeJCP5wVD\nx17fgFtBiOLSGOCbzZhdGTjBFLCtFObUoH9N8rpn3X3XnIHNcS6L5b+kFsBk7HnJ\nYICkpc+O1LgRYjLN+3D1MAmKtESujrV1gAs7jenG+DYHxMqeGA0yzsFH22eSufZl\nermWTiN5i9zL9lmVzho0ntx67Wv6gtfR0gKO8F/JHYC7x1oZog81ETfXebPwekGN\nLcS1qJYfU4E+4ytV/T6u7YhqrMKkOk9QvybEqzpkotEPmakcZUfO6GjQH673yEo7\ndV0ZJwSQQp+q+k/yTezxi+brqPWBCxeVzV7/y9xVpgIFGc1NOiM2deORIB5htG8V\nYjpZOlEcZbi30JGY2T7nhy1nH168WO4Pmrd0HLYLV8oXNEZswzwc4gtTQUru+3DB\nUTx9aVu+K8O/fDaB93M9VgQIG7cjNIc20ZHuRThd1F0OubyH4PbGkEGASjaW+JzA\nR9x/7KUnKHFvR7uv1se7bH6prEVj9hn8+26qMQuS+EI5Vkx02YQVIP/jxOF2+kc3\ngAWrveFaPNR/g09BM6TSvq1sKtAfD6m9sMGMk1jUEIPEtZkq2qtispV0zn72WRJb\ngMbsKpyH4WQuPXsDlzhsRrbcuq7+FKjw0OoUyHiZAnvjv+ROkQKCAQEA61bez6ZP\nUq4m7DF5s0R877vElVUA9BLP21or+XHtzCizuedifkBm87yMu8TDa9rA40ASDv5x\ne9UKHnuDoQN9W8oGv5+cuXnarq9ovgKCxupo8EdXiLtMGNZYf0k2fHb9qYSWRvKZ\n7o6IkQ220og6x/cFRNroibRe5jiWXzGbrQ1bpq2BQMH7Vjlu7MXz1FY1MulqNuWc\nuAaS0H96LiSwDjhpZYsfxIPJMFMBO8AVitcTqjx3zfD9Q5pqG7Tm1/lUKnGzMxn/\nEaHKY5Ynjalw1F4IQrvAF7ticUQrCQlXFs9+aj2XsFAa8KmlycpqmQwaCmPVk2uX\n1hmHznStM4ukkwKCAQEA6u8ZpkLCC1T1ZHDs4mORtYta6eDfgJC9+rpUiHG2i3QM\njuT+xx4CEarEYh9OIJOoZnVg2C+yJoe8Cj5Zfao0riymEw5Pjbl98qgg7wNO/TTp\nUJ9FTYbNKIIFKzFgohAyP0/wNtU6LtAAg96r392ZflvmYvDvn4blYKsGsH1hH//g\n2MkeZP5kbV4wwymeyGKmYY+4V2AuFD0HfDdwPwYsOajBmlt07szWFQRHwfLXcGen\nVUHiZKoMlcmY+zlXojCGaYF6O1y4XSUQmZk4RrICA2p21Y0IQEZG6MQUrhh9Pe7m\nz3n2BcqqXOY8nNOMuYCTW4Fg8g/Zbcooue3XrKqJfwKCAQEAq6AUqbaNjqx2sjV6\nnWPLAUIcIRN2uIinkT53Z1jQpNziBSdbSttIuWO99omOQq+rj48AHHMu67H3yTcm\nTVwwJap1tXV/ZDwbYN2HM1q9aLc2hFRi39JHsq1OqLUJyUc1wlRxC5iM7c6AhdhM\nhcjXpVDeRNKoVxR1SMvnzdBtyYw+VJWf90wAMWd9H6tVIHsmHNTgotxcqFAS7X6p\n6dDTm+tD4cvOBRBevqEp+xsgGrqzGqkybbvb11tbGo2Y89Rerv1wREonHAKziGDO\nw57YK+fPemtVsXj0V8nw4IwxOO9lL7FvWCNzIiN2Rm0ZxDdxHjakYPhMKNVBlyy7\nyyWBhQKCAQEAzGNntTWvC5zGQLPd4rctNPwgBAkXaQqDvXYNxQbpATndExQ2c5Sv\nyB8A6zzBA6Zyg3Dq6CnWm57LAT5tLN6/JijB8yIoI+yUnl+XB1mhhrhZo0JtUJ1O\nL0eup301zMi6bkplec7X55IeYJuAfCqWN1BMhxeF4QvC0u73yuPJc8VxICkYwFRx\nmpSvZ5r2AO+SQJ6AicOrUwAlWvQAO1RrdV5T0K5wdLDQL0iiBSH4PFpxQcMn6tvm\nRwo0hZvFmd0NkwyKAssPujlcz8R1KUwEZGRjLjacTw7U8fxk4XF8xDzroOVCHJfM\nzzWe1fFWJKyvhpRweyc+yj1uYROC5z0WkQKCAQEA3FygaAmdFQkHURRomvRZ1hab\nhjoaQJpqg5/Tu+aH96/cTI5MGjz7tuZGOuHZq2ki+lIJZSbq2jytZCJBz4Of4JsJ\n7zBGYTNkou5gOvjS0EyxirUlr5hT+tn/tfBv988wiY4OpTFWyIj1DXZrY7y9kdPW\nutYYiFmwV/FWePzNuiJnQghT2tBWayEPlsUZ8lvsinu+oBxEjKVrNYdTu1E8rjaX\ndOewDMt9+/rsWo4kllhiA7Jk8IS03dNiT24UK8bjFa/QZZYkktyhOZ0/wqCekMVQ\nTVIAkxwDntDWruZF4ohO1G1yAq6kxdD4b5/7dD82SKoZbbwN21xGpqAjB4Rw1Q==\n-----END RSA PRIVATE KEY-----\n"

      "postgres_password": "password",

      "django_allowed_hosts": ".zeub-web.com",
      "django_admin_url": "r'^admin/'",
      "django_django_settings_module": "config.settings.production",
      "django_secret_key": "jz04wx=tp_70&r-zdqh%tv5g7y*a7+=qh9p9)%&bhb37zpe&og",

      "django_aws_s3_host": "sos.exo.io",
      "django_aws_storage_bucket_name": "%(bucket)s.sos.exo.io",
      "django_aws_secret_access_key": ""
      "django_aws_storage_bucket_name": ""

      "django_mailgun_api_key": ""
      "django_server_email": ""
      "mailgun_sender_domain": ""
      "django_sentry_dsn": "https://9b3fbbc4bd7b41e39de7951001c4eb3e:44fc254dcc4d4781b2c09c80391aaaf0@app.getsentry.com/76067",
    }
* Then create a task template with **deploy.yaml** for playbook name and run it.

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run manage.py test
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ py.test

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html



Celery
^^^^^^

This app comes with Celery.

To run a celery worker:

.. code-block:: bash

    cd zeub_web
    celery -A zeub_web.taskapp worker -l info

Please note: For Celery's import magic to work, it is important *where* the celery commands are run. If you are in the same folder with *manage.py*, you should be right.




Email Server
^^^^^^^^^^^^

In development, it is often nice to be able to see emails that are being sent from your application. For that reason local SMTP server `MailHog`_ with a web interface is available as docker container.

.. _mailhog: https://github.com/mailhog/MailHog

Container mailhog will start automatically when you will run all docker containers.
Please check `cookiecutter-django Docker documentation`_ for more details how to start all containers.

With MailHog running, to view messages that are sent by your application, open your browser and go to ``http://127.0.0.1:8025``




Sentry
^^^^^^

Sentry is an error logging aggregator service. You can sign up for a free account at  https://getsentry.com/signup/?code=cookiecutter  or download and host it yourself.
The system is setup with reasonable defaults, including 404 logging and integration with the WSGI application.

You must set the DSN url in production.


Deployment
----------

The following details how to deploy this application.



Docker
^^^^^^

See detailed `cookiecutter-django Docker documentation`_.

.. _`cookiecutter-django Docker documentation`: http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html


